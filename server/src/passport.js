const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const GithubStrategy = require("passport-github2").Strategy;
const FacebookStrategy = require("passport-facebook").Strategy;

const GOOGLE_CLIENT_ID =
  "547276967420-9084ft5qahr0o99pjq26e798mbaa9k23.apps.googleusercontent.com";
const GOOGLE_CLIENT_SECRET = "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";

GITHUB_CLIENT_ID = "05cd3c0f479521a12820";
GITHUB_CLIENT_SECRET = "7ba318daf3f8f430df1f76ee452ffa902bc3e88e";

FACEBOOK_APP_ID = "351629563306502";
FACEBOOK_APP_SECRET = "2d6201552bfe854287d4732aae08691d1e305db643532edd9faf6f3e1c426d583de4c7991a45b639f193d9cdbfe9d9395dbae8e5a2858e3d5747d4cd01b6b154";

passport.use(
  new GoogleStrategy(
    {
      clientID: GOOGLE_CLIENT_ID,
      clientSecret: GOOGLE_CLIENT_SECRET,
      callbackURL: "/api/auth/google/callback",
    },
    function (accessToken, refreshToken, profile, done) {
      done(null, profile);
    }
  )
);

passport.use(
  new GithubStrategy(
    {
      clientID: GITHUB_CLIENT_ID,
      clientSecret: GITHUB_CLIENT_SECRET,
      callbackURL: "/api/auth/github/callback",
    },
    function (accessToken, refreshToken, profile, done) {
      done(null, profile);
    }
  )
);

passport.use(
  new FacebookStrategy(
    {
      clientID: FACEBOOK_APP_ID,
      clientSecret: FACEBOOK_APP_SECRET,
      callbackURL: "/api/auth/facebook/callback",
    },
    function (accessToken, refreshToken, profile, done) {
      done(null, profile);
    }
  )
);

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});

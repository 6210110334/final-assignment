const winston = require("winston");
const expressWinston = require("express-winston");
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const axios = require("axios");
const cors = require("cors");
const cookieSession = require("cookie-session");
const passport = require("passport");
const passportSetup = require("./passport");
const authRoute = require("./routes/auth");
const articlesRoute = require("./routes/articles");
const postsRoute = require("./routes/posts");
const port = 8000;

app.use(
  cookieSession({
    name: "session",
    keys: ["fn"],
    maxAge: 24 * 60 * 60 * 100,
  })
);

app.use(passport.initialize());
app.use(passport.session());

app.use(
  cors({
    origin: "https://dc16.coe.psu.ac.th",
    methods: "GET,POST,PUT,DELETE",
    credentials: true,
  })
);

app.use("/api/auth", authRoute);
app.use("/api/articles", articlesRoute);
app.use("/api/posts", postsRoute);

app.use(
  expressWinston.logger({
    transports: [new winston.transports.Console()],
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.json()
    ),
    meta: false,
    msg: "HTTP  ",
    expressFormat: true,
    colorize: false,
    ignoreRoute: function (req, res) {
      return false;
    },
  })
);

app.listen(port, () => {
  console.log(`app listening on port ${port}`);
});

import Card from "../components/Card";

function Home({articles}) {
  return (
    <div className="home">
      {articles.map((article) => (
        <Card article={article}></Card>
      ))}
    </div>
  );
}

export default Home;

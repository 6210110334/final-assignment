import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
import config from "../config";

function Article() {
  const [article, setArticle] = useState(null);
  const location = useLocation();
  const id = location.pathname.split("/")[2];

  useEffect(() => {
    const reqA = async () => {
      let result = await axios.get(`${config.apiUrlPrefix}/articles/${id}`);
      setArticle(result.data);
    };
    reqA();
  }, []);

  return article ? (
    <div className="article">
      <img src={article.img} alt="" className="articleImag"></img>
      <h1 className="articleTitle">{article.title}</h1>
      <p className="articleDesc">{article.desc}</p>
      <p className="articleLongDesc">{article.longDesc}</p>
    </div>
  ) : (
    <div></div>
  );
}
export default Article;

import { Link } from "react-router-dom";

function Card({ article }) {
  return (
    <div className="card">
      <Link className="link" to={`/article/${article.id}`}>
        <span className="title">{article.title}</span>
        <img src={article.img} alt="" className="img"></img>
        <p className="desc">{article.desc}</p>
        <button className="cardButton">Read More</button>
      </Link>
    </div>
  );
}
export default Card;

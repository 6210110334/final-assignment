import "./App.css";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Login from "./pages/Login";
import Navbar from "./components/Navbar";
import Home from "./pages/Home";
import Article from "./pages/Article";
import { useEffect, useState } from "react";
import axios from "axios";
import config from "./config";

function App() {
  const [user, setUser] = useState(null);
  const [articles, setArticles] = useState(null);

  useEffect(() => {
    const getUser = () => {
      fetch(`${config.apiUrlPrefix}/auth/login/success`, {
        method: "GET",
        credentials: "include",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "Access-Control-Allow-Credentials": true,
        },
      })
        .then((response) => {
          if (response.status === 200) return response.json();
          throw new Error("authentication has been failed!");
        })
        .then((resObject) => {
          setUser(resObject.user);
        })
        .catch((err) => {
          console.log(err);
        });
    };

    const reqArticles = async () => {
      let result = await axios.get(`${config.apiUrlPrefix}/articles`);
      setArticles(result.data);
    };
    reqArticles();
    getUser();
  }, []);

  return (
    <BrowserRouter>
      <div>
        <Navbar user={user} />
        <Routes>
          <Route
            path="/"
            element={articles ? <Home articles={articles}></Home> : <div></div>}
          ></Route>
          <Route
            path="/login"
            element={user ? <Navigate to="/" /> : <Login />}
          ></Route>
          <Route
            path="/article/:id"
            element={user ? <Article /> : <Navigate to="/login" />}
          ></Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
